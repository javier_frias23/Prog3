from Robot import Robot
if __name__ == "__main__":
    print("LIMPIA CASAS")

    r = input("\nNombre del Robot: ")
    robot = Robot(r)

    while True:
        if robot.nivel_bateria == 10:
            print("Me quedé sin batería, necesito recargarme")
            break

        print("\n\nMenu - Robot - " + robot.nombre)
        print("1. Barrer")
        print("2. Fregar")
        print("4. Calcular operaciones básicas")
        print("5. Revisar refrigerador")
        print("6. Ver nivel de bateria")
        print("7. Salir")

        try:
            o = int(input("\nOpcion: "))
        except:
            o = -15
        if o == 1:
            print(robot.barrer())
        elif o == 2:
            print(robot.fregar())
        elif o == 3:
            robot.hablar()
        elif o == 4:
            print(robot.calcular_operaciones_basicas())
        elif o == 5:
            print(robot.revisar_refrigerador())
        elif o == 6:
            print(robot.nivel_bateria)
        elif o == 7:
            print("Ciao")
            break
        else:
            print("Opcion invalida")

